# Use an official Python runtime as a parent image
FROM python:3.8

# Work dir
WORKDIR /app

# Copy files   
ADD . /app

# Install any needed packages specified in requirements.txt
RUN python setup.py develop

# Make port 8000 available to the world
EXPOSE 8080
