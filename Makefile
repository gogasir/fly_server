PROJECT_NAME=fly

all: run

run:
	@docker-compose up

stop:
	@docker-compose stop

down:
	@docker-compose down

build:
	@docker-compose build

build_force:
	@docker-compose build --force-rm

ps:
	@docker-compose ps

init_db:
	@docker exec -it fly_app /usr/local/bin/python fly/init_db.py

update:
	@docker exec -it fly_app /usr/local/bin/python fly/update.py

check:
	@docker exec -it fly_app /usr/local/bin/python fly/check_flights.py

psql:
	@docker exec -it postgres_app psql -U postgres
