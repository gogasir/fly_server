Run docker
    `make all`

The first time you start up, you need to initialise the database
    `make init_db`

Search for the low price for the month ahead. 
    `make update`

Confirmation of prices
    `make check`

Run psql
    `make psql`

