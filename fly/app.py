from aiohttp import web

from fly.db import close_pg, init_pg
from fly.routes import setup_routes
from fly.settings import config


async def my_web_app():
    app = web.Application()
    setup_routes(app)
    app['config'] = config
    app.on_startup.append(init_pg)
    app.on_cleanup.append(close_pg)
    # web.run_app(app)
    return app
