import asyncio
import logging
import ssl
import urllib.parse

import aiohttp
from aiopg.sa import create_engine

from fly import db
from fly.settings import config

DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"
URL_CHECK_FLIGHTS = \
    "https://booking-api.skypicker.com/" \
    "api/v0.1/check_flights?v=2&bnum=3" \
    "&pnum=2&affily=picky_{market}&currency=EUR&adults=1&children=0&infants=0"


async def fetch(session, url):
    async with session.get(url, ssl=ssl.SSLContext()) as response:
        return await response.json()


async def fetch_all(param_urls, param_loop):
    async with aiohttp.ClientSession(loop=param_loop) as session:
        return await asyncio.gather(
            *[fetch(session, url) for url in param_urls],
            return_exceptions=True)


async def get_flights():
    """Load flights for price check

    Returns:
        [type]: [description]
    """
    async with create_engine(db_url) as engine:
        async with engine.acquire() as conn:
            return await db.get_check_flights(conn)


async def set_flights_checked(booking_token: str):
    """ Update lights_checked """
    async with create_engine(db_url) as engine:
        async with engine.acquire() as conn:
            return await db.set_flights_checked(conn, booking_token)


async def set_flights_checked_price(price: float, booking_token: str) -> None:
    """ Update lights_checked and price

    Args:
        price (float): new price
        booking_token (str): Bookin Tocken

    Returns:
        [type]: [description]
    """
    async with create_engine(db_url) as engine:
        async with engine.acquire() as conn:
            return await db.set_flights_checked_price(conn, price,
                                                      booking_token)


async def run(current_loop):
    """ Find flights for check and send recust to kiwi
    Args:
        current_loop (AbstractEventLoop): EventLoop
    """
    logging.debug('Find flights for check...')
    result = await get_flights()
    logging.debug('Request service...')
    urls = [
        URL_CHECK_FLIGHTS + '&booking_token=' +
        urllib.parse.quote(element['booking_token']) for element in result
    ]
    elements = await fetch_all(urls, current_loop)
    logging.debug('Update database...')
    for element in elements:
        if not element.get('flights_invalid') and element.get(
                'flights_checked'):
            if element.get('price_change'):
                await set_flights_checked_price(element.get('flights_price'),
                                                element.get('booking_token'))
            else:
                await set_flights_checked(element.get('booking_token'))
    logging.debug('End')


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    db_url = DSN.format(**config['postgres'])

    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
