import aiopg.sa

from sqlalchemy import (
    MetaData, Table, Column,
    Integer, String, Boolean, Float,
    Date, DateTime, and_)

__all__ = ['table_flight']

meta = MetaData()

table_flight = Table(
    'flight', meta,

    Column('id', Integer, primary_key=True, index=True),
    Column('departure_date', Date, nullable=False),
    Column('departure_date_time', DateTime, nullable=False),
    Column('fly_from', String(3), nullable=False),
    Column('fly_to', String(3), nullable=False),
    Column('booking_token', String, unique=True, nullable=False, index=True),
    Column('price', Float, nullable=False),
    Column('check_flights', Boolean, nullable=False, default=True)
)


async def init_pg(app):
    conf = app['config']['postgres']
    engine = await aiopg.sa.create_engine(
        database=conf['database'],
        user=conf['user'],
        password=conf['password'],
        host=conf['host'],
        port=conf['port'],
        minsize=conf['minsize'],
        maxsize=conf['maxsize'],
    )
    app['db'] = engine


async def close_pg(app):
    app['db'].close()
    await app['db'].wait_closed()


async def get_flight_all(conn):
    """ Find all flight """
    result = await conn.execute(table_flight.select())
    flight_records = await result.fetchall()
    return flight_records


async def get_flight(conn, departure_date: String, fly_from: String, fly_to: String):
    """ Find flight by booking token """
    result = await conn.execute(table_flight.select().where(
        and_(table_flight.c.departure_date == departure_date,
             table_flight.c.fly_from == fly_from,
             table_flight.c.fly_to == fly_to)
    ))
    flight_records = await result.first()
    return flight_records


async def update_flight(conn, item: dict):
    await conn.execute(
        table_flight.update().where(
            and_(
                table_flight.c.departure_date == item.get('departure_date'),
                table_flight.c.fly_from == item.get('fly_from'),
                table_flight.c.fly_to == item.get('fly_to')
            )
        ).values(
            departure_date_time=item.get('departure_date_time'),
            price=item.get('price'),
            check_flights=item.get('check_flights'),
            booking_token=item.get('booking_token')
        )
    )


async def set_flights_checked(conn, booking_token: str):
    """ Update  table_flight set check_flights=True """
    await conn.execute(
        table_flight.update().where(
            table_flight.c.booking_token == booking_token,
        ).values(
            check_flights=True,
        )
    )


async def set_flights_checked_price(conn, price: float, booking_token: str):
    """ Update  table_flight set check_flights=True and price"""
    await conn.execute(
        table_flight.update().where(
            table_flight.c.booking_token == booking_token,
        ).values(
            price=price,
            check_flights=True,
        )
    )


async def get_check_flights(conn):
    """ Find flight for check flight """
    result = await conn.execute("SELECT * FROM flight")
    return await result.fetchall()


async def insert_flight(conn, item: dict):
    await conn.execute(table_flight.insert().values(**item))


async def get_flight_by_date(conn, departure_date):
    result = await conn.execute(table_flight.select().where(table_flight.c.departure_date == departure_date))
    flight_records = await result.fetchall()
    return flight_records
