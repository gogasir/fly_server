from sqlalchemy import create_engine, MetaData

from fly.db import table_flight
from fly.settings import config

DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"


def create_tables(param_engine):
    meta = MetaData()
    meta.create_all(bind=param_engine, tables=[table_flight, ])


if __name__ == '__main__':
    db_url = DSN.format(**config['postgres'])
    engine = create_engine(db_url)
    create_tables(engine)
