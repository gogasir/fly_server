import pathlib
import yaml

BASE_DIR = pathlib.Path(__file__).parent.parent
config_path = BASE_DIR / 'fly' / 'config' / 'fly.yaml'


def get_config(path):
    with open(path) as f:
        my_config = yaml.safe_load(f)
    return my_config


config = get_config(config_path)
