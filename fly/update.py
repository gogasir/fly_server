import asyncio
import logging
import ssl
from datetime import date
from datetime import datetime
from itertools import groupby

import aiohttp
import urllib3
from dateutil.relativedelta import relativedelta
from aiopg.sa import create_engine

from fly import db
from fly.settings import config

urllib3.disable_warnings()

DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"
FORMAT_DATE = "%Y-%m-%d"
FORMAT_DATE_TIME = "%Y-%m-%d, %H:%M:%S"
START_DATE = date.today().strftime('%d/%m/%Y')
END_DATE = (date.today() + relativedelta(months=1)).strftime('%d/%m/%Y')
URL_FLIGHTS = f'https://api.skypicker.com/flights?curr=EUR&adults=1&partner=picky&date_from={START_DATE}&date_to={END_DATE}'
LOCATION = [
    {'ALA': 'TSE'},
    {'TSE': 'ALA'},
    {'ALA': 'MOW'},
    {'MOW': 'ALA'},
    {'ALA': 'CIT'},
    {'CIT': 'ALA'},
    {'TSE': 'MOW'},
    {'MOW': 'TSE'},
    {'TSE': 'LED'},
    {'LED': 'TSE'},
]


async def fetch(session, param_url):
    request_url = URL_FLIGHTS + ''.join([f'&fly_from={key}&fly_to={value}' for key, value in param_url.items()])
    async with session.get(request_url, ssl=ssl.SSLContext()) as response:
        return await response.json()


async def fetch_all(param_urls, param_loop):
    async with aiohttp.ClientSession(loop=param_loop) as session:
        results = await asyncio.gather(*[fetch(session, url) for url in param_urls], return_exceptions=True)
        return results


def date_from_int(data_time: int) -> str:
    return datetime.fromtimestamp(data_time).strftime(FORMAT_DATE)


def extract_data(response_json) -> dict:
    list_items = response_json.get("data", [])

    result_dict = {}
    for dt, k in groupby(
            sorted(list_items, key=lambda x: date_from_int(x['dTimeUTC'])),
            key=lambda x: date_from_int(x['dTimeUTC'])):
        row_min_price = min(k, key=lambda x: x['price'])
        result_dict[dt] = {}
        result_dict[dt]['departure_date'] = dt
        result_dict[dt]['departure_date_time'] = datetime.fromtimestamp(row_min_price['dTimeUTC']).date()
        result_dict[dt]['fly_from'] = row_min_price['flyFrom']
        result_dict[dt]['fly_to'] = row_min_price['flyTo']
        result_dict[dt]['booking_token'] = row_min_price['booking_token']
        result_dict[dt]['price'] = row_min_price['price']
        result_dict[dt]['check_flights'] = False

    return result_dict


async def save_flights(flights: dict):
    async with create_engine(db_url) as engine:
        async with engine.acquire() as conn:
            for item in flights.items():
                flight_db = await db.get_flight(
                    conn,
                    item[1].get("departure_date"),
                    item[1].get("fly_from"),
                    item[1].get("fly_to"),
                )
                if flight_db:
                    await db.update_flight(conn, item[1])
                else:
                    await db.insert_flight(conn, item[1])


async def run(current_loop):
    logging.debug('Start request url...')
    result = await fetch_all(LOCATION, current_loop)
    logging.debug('Update database...')
    for item in result:
        data = extract_data(item)
        await save_flights(data)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    db_url = DSN.format(**config['postgres'])

    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
