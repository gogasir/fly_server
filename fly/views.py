import json
from datetime import datetime

from aiohttp import web

from fly import db


async def index(request):
    async with request.app['db'].acquire() as conn:
        cursor = await conn.execute(db.table_flight.select().order_by('departure_date'))
        records = await cursor.fetchall()
        item = [dict(q) for q in records]
        return web.json_response(json.dumps(item, default=date_converter))


def date_converter(o):
    if isinstance(o, datetime):
        return "{}-{}-{}".format(o.year, o.month, o.day)
