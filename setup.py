import os
import re

from setuptools import find_packages, setup

REGEXP = re.compile(r"^__version__\W*=\W*'([\d.abrc]+)'")


def read_version():
    init_py = os.path.join(os.path.dirname(__file__), 'fly', '__init__.py')

    with open(init_py) as f:
        for line in f:
            match = REGEXP.match(line)
            if match is not None:
                return match.group(1)
        else:
            msg = f'Cannot find version in ${init_py}'
            raise RuntimeError(msg)


install_requires = [
    'aiohttp==3.7.3',
    'SQLAlchemy==1.3.23',
    'PyYAML==5.4.1',
    'psycopg2-binary==2.8.6',
    'gunicorn==20.0.4',
    'aiopg==1.1.0',
    'python-dateutil==2.8.1',
    'urllib3==1.26.3'
]

setup(
    name='fly',
    version=read_version(),
    description='Low airfare calendar',
    platforms=['POSIX'],
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    zip_safe=False,
)
